from django.http import HttpResponseRedirect
from django.shortcuts import reverse
from django.views.generic.base import TemplateView


class MainView(TemplateView):
    template_name = 'main/main.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('account:account'))
        return super(MainView, self).dispatch(request, *args, **kwargs)
