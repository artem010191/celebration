from django.views.generic import View
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect
from django.shortcuts import reverse
from apps.token.utils import set_token_cookies


class ActivationView(View):
    def dispatch(self, request, *args, **kwargs):
        access_token = request.GET.get('access_token')
        refresh_token = request.GET.get('refresh_token')

        if access_token and refresh_token:
            user = authenticate(request, access_token=access_token)

            if not user.is_active:
                user.is_active = True
                user.save()

            response = HttpResponseRedirect(reverse('account:account'))
            response = set_token_cookies(response, user, access_token, refresh_token)
            return response
        return HttpResponseRedirect(reverse('login'))
