from django.urls import path
from .views import ActivationView


urlpatterns = [
    path('', ActivationView.as_view(), name='activation')
]
