from django.conf import settings
from django.db import models
from django.utils import timezone
from .settings import REMINDER_OPTIONS
from .managers import EventFilterManager
import datetime


class EventsModel(models.Model):
    event_name = models.CharField(verbose_name='Event name', max_length=100)
    event_start = models.DateTimeField(verbose_name='Event start')
    event_end = models.DateTimeField(verbose_name='Event end', blank=True)
    reminders = models.SmallIntegerField(verbose_name='Reminder', choices=REMINDER_OPTIONS, null=True, blank=True)
    active = models.BooleanField(verbose_name='Active', default=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='User', related_name='events', on_delete=models.DO_NOTHING)

    objects = models.Manager()
    event_filter = EventFilterManager.as_manager()

    def save(self, *args, **kwargs):
        if not self.event_end:
            self.event_end = self.event_start.replace(hour=0, minute=0, microsecond=0) + datetime.timedelta(days=1)

        self._disable_event()
        return super(EventsModel, self).save(*args, **kwargs)

    def _disable_event(self):
        if self.event_end < timezone.localtime(timezone.now()):
            self.active = False

    class Meta:
        db_table = 'events_events'
