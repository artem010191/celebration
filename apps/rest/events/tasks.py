from celery import shared_task
from apps.email.functions import send_email
from apps.rest.events.models import EventsModel


@shared_task
def notify_user_about_event_task(context):
    EventsModel._default_manager.filter(id=context['event_id']).update(active=False)
    send_email(email_name='notification_about_event', context=context, to=(context['email'],))
