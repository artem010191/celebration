import datetime


REMINDER_OPTIONS = (
    (0, 'For hour'),
    (1, 'For 2 hours'),
    (2, 'For 4 hours'),
    (3, 'For day'),
    (4, 'For week')
)

DILAY_OPTIONS = {
    0: datetime.timedelta(hours=1),
    1: datetime.timedelta(hours=2),
    2: datetime.timedelta(hours=4),
    3: datetime.timedelta(days=1),
    4: datetime.timedelta(weeks=1)
}
