from django.db.models.signals import post_save
from django.dispatch import receiver
from apps.rest.events.tasks import notify_user_about_event_task
from .models import EventsModel
from .settings import DILAY_OPTIONS


@receiver(post_save, sender=EventsModel)
def set_event_notification_receiver(sender, instance, **kwargs):
    if instance.active:
        exec_time = instance.event_start - DILAY_OPTIONS[instance.reminders] if instance.reminders != None else instance.event_start
        context = {
            'event_id': instance.id,
            'email': instance.user.email,
            'event_name': instance.event_name,
            'event_start': instance.event_start,
            'event_end': instance.event_end
        }
        notify_user_about_event_task.apply_async((context,), eta=exec_time, expires=instance.event_end)
