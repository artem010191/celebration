from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.response import Response
from .serializers import EventSerializer, EVentsByDateSerializer
from .models import EventsModel


class EventsView(CreateAPIView):
    serializer_class = EventSerializer

    def get_queryset(self):
        return EventsModel._default_manager.filter(user=self.request.user).all()


class EventsListByDateView(ListAPIView):
    serializer_class = EVentsByDateSerializer

    def get_queryset(self):
        return EventsModel._default_manager.all()

    def list(self, request, *args, **kwargs):
        if request.query_params:
            serializer = self.get_serializer(data=request.query_params)

            if serializer.is_valid():
                month = serializer.data.get('month')
                day = serializer.data.get('day')
                if month:
                    response = {
                        i.event_name: {
                            'event_start': i.event_start,
                            'event_end': i.event_end
                        } for i in EventsModel.event_filter.events_for_month(month)
                    }
                    return Response(response)
                response = {
                    i.event_name: {
                        'event_start': i.event_start,
                        'event_end': i.event_end
                    } for i in EventsModel.event_filter.events_for_day(day)
                }
                return Response(response)
            return Response(serializer.errors)
        return Response(status=200)
