from django.urls import path
from .views import EventsView, EventsListByDateView


urlpatterns = [
    path('events/', EventsView.as_view()),
    path('events-by-date/', EventsListByDateView.as_view())
]
