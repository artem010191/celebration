from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from .models import EventsModel
import datetime


class EventSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        return super(EventSerializer, self).create(validated_data)

    class Meta:
        model = EventsModel
        exclude = ['user', 'active']


class EVentsByDateSerializer(serializers.Serializer):
    month = serializers.CharField(max_length=20, required=False)
    day = serializers.CharField(max_length=20, required=False)

    def validate_month(self, attr):
        try:
            datetime.datetime.strptime(attr, '%Y-%m')
        except:
            raise ValidationError('Date format must be %Y-%m.')
        return attr

    def validate_day(self, attr):
        try:
            datetime.datetime.strptime(attr, '%Y-%m-%d')
        except:
            raise ValidationError('Date format must be %Y-%m-%d.')
        return attr

    def validate(self, attrs):
        month = attrs.get('month')
        day = attrs.get('day')
        if day and month:
            raise ValidationError('Set only month or day parameter')
        return attrs
