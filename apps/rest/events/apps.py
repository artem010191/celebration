from django.apps import AppConfig


class EventsAppConfig(AppConfig):
    name = 'apps.rest.events'
    label = 'events'

    def ready(self):
        import apps.rest.events.receivers
