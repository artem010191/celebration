from django.db import models
import datetime


class EventFilterManager(models.QuerySet):
    def events_for_day(self, date):
        range_start = datetime.datetime.strptime(date, '%Y-%m-%d')
        range_end = range_start + datetime.timedelta(days=1)
        return self.model._default_manager.filter(event_start__range=(range_start, range_end), active=True)

    def events_for_month(self, date):
        date = datetime.datetime.strptime(date, '%Y-%m')
        return self.model._default_manager.filter(event_start__year=date.year, active=True).filter(event_start__month=date.month)
