from django.urls import path
from .views import TokenPairView


urlpatterns = [
    path('token-pair/', TokenPairView.as_view(), name='token_pair')
]
