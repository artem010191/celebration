from rest_framework.views import APIView
from rest_framework.response import Response
from apps.token.utils import get_token_pair, decode_token
from apps.token.signals import rest_token_getting


class TokenPairView(APIView):
    def post(self, request, format=None):
            payload = {
                'username': request.user.username,
                'password': request.user.password,
                'email': request.user.email
            }
            token_pair = get_token_pair(payload)

            rest_token_getting.send(sender=self.__class__, user=request.user, refresh_token=token_pair['refresh_token'])

            return Response({'access_token': token_pair['access_token'], 'refresh_token': token_pair['refresh_token']})
