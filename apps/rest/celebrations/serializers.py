from rest_framework import serializers


class CelebrationsSerializer(serializers.Serializer):
    month = serializers.IntegerField(max_value=12)
