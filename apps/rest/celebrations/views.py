from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from requests import get
from ics import Calendar
from .serializers import CelebrationsSerializer


class CelebrationsView(GenericAPIView):
    serializer_class = CelebrationsSerializer

    def get(self, request, format=None):
        data = request.query_params.get('month')

        if data:
            serializer = self.get_serializer(data={'month': data})

            if serializer.is_valid():
                serializer_data = serializer.data
                celebrations_data = Calendar(get(
                    'https://www.officeholidays.com/ics/ics_country.php?tbl_country={country_name}'.format(
                        country_name=request.user.country.name
                    )).text)
                events_for_month = [
                        i for i in celebrations_data.events if i.begin.datetime.month == serializer_data['month']
                    ]
                response_data = {
                   i.name: {'begin': i.begin.humanize(), 'end': i.end.humanize()} for i in events_for_month
                }
                return Response(response_data)
            else:
                return Response(serializer.errors)
        return Response(status=200)
