from django.urls import path
from .views import CelebrationsView


urlpatterns = [
    path('celebrations/', CelebrationsView.as_view(), name='celebrations')
]
