from django.urls import path, include
from .events import urls as events_urls
from .celebrations import urls as celeb_urls
from .token_pair import urls as tokpair_urls


app_name = 'rest'
urlpatterns = [
    path('', include(events_urls)),
    path('', include(celeb_urls)),
    path('', include(tokpair_urls))
]
