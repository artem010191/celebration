from django.contrib.auth import get_user_model
from rest_framework import authentication
from apps.token.utils import decode_token
from apps.token.models import RefreshTokensModel


class TokenAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        auth_header = request.META.get('HTTP_AUTHORIZATION')

        if auth_header:
            auth_header = auth_header.split(' ')
            access_token = auth_header[1] if auth_header[0] == 'Beaver' else None

            if access_token:
                user_model = get_user_model()
                try:
                    payload = decode_token(access_token)
                    user = user_model._default_manager.filter(
                        username=payload['username'],
                        password=payload['password'],
                        email=payload['email']
                    ).first()
                except:
                    return None

                if user:
                    return (user, None)
        return None


class RefreshTokenAuthentication(authentication.BasicAuthentication):
    def authenticate(self, request):
        auth_header = request.META.get('HTTP_AUTHORIZATION')

        if auth_header:
            auth_header = auth_header.split(' ')
            refresh_token = auth_header[1] if auth_header[0] == 'Beaver' else None

            if refresh_token:
                try:
                    decode_token(refresh_token)
                except:
                    return None

                token_obj = RefreshTokensModel._default_manager.filter(refresh_token=refresh_token, active=True).first()
                user = token_obj.user if token_obj else None

                if user:
                    return (user, None)
        return None
