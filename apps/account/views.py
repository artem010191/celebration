from django.views.generic import TemplateView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import reverse
from apps.rest.events.models import EventsModel
from .forms import EventsForm


class BaseView(LoginRequiredMixin):
    def get_login_url(self):
        return reverse('login')


class AccountView(BaseView, TemplateView):
    template_name = 'account/account.html'


class EventsView(BaseView, CreateView):
    template_name = 'account/events.html'
    model = EventsModel
    form_class = EventsForm
    
    def form_valid(self, form):
        self.object = form.save()
        self.object.user = self.request.user
        self.object.save()
        return super(EventsView, self).form_valid(form)
    
    def get_success_url(self):
        return reverse('account:events')

    def get_queryset(self):
        super(EventsView, self).get_queryset()
        return self.model._default_manager.filter(user=self.request.user, active=True).all()

    def get_context_data(self, **kwargs):
        context = super(EventsView, self).get_context_data(**kwargs)
        context['objects'] = self.get_queryset()
        return context
