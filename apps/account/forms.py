from django import forms
from apps.rest.events.models import EventsModel


class EventsForm(forms.ModelForm):
    event_start = forms.Field(widget=forms.SplitDateTimeWidget(date_attrs={'type': 'date'}, time_attrs={'type': 'time'}))
    event_end = forms.Field(required=False, widget=forms.SplitDateTimeWidget(date_attrs={'type': 'date'}, time_attrs={'type': 'time'}))

    def clean_event_start(self):
        data = self.cleaned_data['event_start']
        event_start = ' '.join(data)
        return event_start

    def clean_event_end(self):
        data = self.cleaned_data['event_end']
        event_end = ' '.join(data)

        if event_end == ' ':
                return None
        return event_end

    def save(self, commit=False):
        return super(EventsForm, self).save(commit)

    class Meta:
        model = EventsModel
        exclude = ['user', 'active']
