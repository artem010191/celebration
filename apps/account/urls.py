from django.urls import path
from .views import AccountView, EventsView


app_name = 'account'
urlpatterns = [
    path('', AccountView.as_view(), name='account'),
    path('events/', EventsView.as_view(), name='events')
]
