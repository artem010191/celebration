from django.apps import AppConfig


class TokenAppConfig(AppConfig):
    name = 'apps.token'
    label = 'token'

    def ready(self):
        import apps.token.receivers
