from django.dispatch import receiver
from .models import RefreshTokensModel
from .signals import setting_token_cookies_signal, rest_token_getting


@receiver(signal=[setting_token_cookies_signal, rest_token_getting])
def user_activation_receiver(sender, user, refresh_token, **kwargs):
    user_tokens = RefreshTokensModel._default_manager.filter(user=user).all()
    user_token = user_tokens.filter(refresh_token=refresh_token).first()

    if user_token and not user_token.active:
        return None
    elif not user_token:
        for i in user_tokens.filter(active=True):
            i.active = False
            i.save()

        return RefreshTokensModel._default_manager.create(user=user, active=True, refresh_token=refresh_token)
