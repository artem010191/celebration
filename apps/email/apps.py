from django.apps import AppConfig


class EmailAppConfig(AppConfig):
    name = 'apps.email'
    label = 'email'

    def ready(self):
        import apps.email.receivers
