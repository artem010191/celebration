from django.core.mail import send_mail
from django.template.loader import render_to_string


def send_email(email_name:str, context:dict, user=None, to=(), from_email='Mailing server'):
    subject = render_to_string('email/' + email_name + '_subject.txt')
    template = render_to_string('email/' + email_name + '.html', context=context)
    send_mail(subject, template, from_email=from_email, recipient_list=to if not user else (user.email,))
