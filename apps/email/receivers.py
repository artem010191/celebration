from django.db.models.signals import post_save
from django.conf import settings
from django.dispatch import receiver
from django.contrib.sites.models import Site
from apps.token.utils import get_token_pair
from .functions import send_email


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def registration_email_receiver(sender, instance, created, *arg, **kwarg):
    if created and not instance.is_superuser:
        payload = {
                'username': instance.username,
                'password': instance.password,
                'email': instance.email,
            }
        token_pair = get_token_pair(payload=payload)

        send_email(
            'confirmation_email',
            {
                'username': instance.username,
                'access_token': token_pair['access_token'],
                'refresh_token': token_pair['refresh_token'],
                'domain': Site.objects.get_current().domain
            },
            user=instance
        )
