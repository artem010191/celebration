from django.contrib.auth import get_user_model
from django.core.validators import EmailValidator
from django import forms
from .validators import email_uniqueness


class RegistrationForm(forms.ModelForm):
    email = forms.EmailField(validators=[EmailValidator, email_uniqueness])

    def save(self, commit=True):
        self.instance.is_active = False
        return super(RegistrationForm, self).save(commit)

    class Meta:
        model = get_user_model()
        fields = ['username', 'password', 'email', 'country']
