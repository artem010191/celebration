from django.apps import AppConfig


class RegistrationAppConfig(AppConfig):
    name = 'apps.registration'
    label = 'registration'
