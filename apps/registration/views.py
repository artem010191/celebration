from django.views.generic import CreateView
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import reverse
from .forms import RegistrationForm


class RegistrationView(SuccessMessageMixin, CreateView):
    form_class = RegistrationForm
    template_name = 'registration/registration.html'
    success_message = 'We just sent activation letter to your email.'

    def get_success_url(self):
        return reverse('registration')
