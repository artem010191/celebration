from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model


def email_uniqueness(value):
    user_model = get_user_model()

    if user_model._default_manager.filter(email=value).first():
        raise ValidationError('User with this email already exists.')
