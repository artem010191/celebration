from django.views.generic import FormView
from django.shortcuts import reverse
from django.http import HttpResponseRedirect
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.messages.views import SuccessMessageMixin
from apps.token.utils import get_token_pair, set_token_cookies
from apps.email.functions import send_email
from .forms import UserLoginForm, LoginEmailForm


class LogUserInView(FormView):
    form_class = UserLoginForm
    template_name = 'login/login.html'

    def get_success_url(self):
        return reverse('account:account')

    def form_valid(self, form):
        response = HttpResponseRedirect(self.get_success_url())
        user = form.user

        payload = {
            'username': user.username,
            'password': user.password,
            'email': user.email
        }
        token_pair = get_token_pair(payload=payload)

        response = set_token_cookies(response, user, token_pair['access_token'], token_pair['refresh_token'])
        return response


class LoginEmailView(SuccessMessageMixin, FormView):
    template_name = 'login/login-email.html'
    form_class = LoginEmailForm
    success_message = 'We just sent activation letter to your email.'

    def get_success_url(self):
        return reverse('login-email')

    def form_valid(self, form):
        payload = {
            'username': form.user.username,
            'password': form.user.password,
            'email': form.user.email
        }
        token_pair = get_token_pair(payload=payload)

        send_email('confirmation_email', context={
            'username': form.user.username,
            'access_token': token_pair['access_token'],
            'refresh_token': token_pair['refresh_token'],
            'domain': get_current_site(self.request).domain
        }, user=form.user)
        return super(LoginEmailView, self).form_valid(form)
