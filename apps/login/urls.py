from django.urls import path
from .views import LogUserInView, LoginEmailView


urlpatterns = [
    path('', LogUserInView.as_view(), name='login'),
    path('login-email/', LoginEmailView.as_view(), name='login-email')
]
