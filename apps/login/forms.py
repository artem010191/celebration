from django import forms
from django.contrib.auth import get_user_model


class UserLoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super(UserLoginForm, self).clean()
        user_model = get_user_model()
        user = user_model._default_manager.filter(
            username=cleaned_data['username'],
            password=cleaned_data['password']
        ).first()

        if not user:
            raise forms.ValidationError(
                'Account with these credentials not exists.'
            )
        elif not user.is_active:
            raise forms.ValidationError(
                'Please active your account for login. '
                'You can click on "send login email" for gain an activation letter.'
            )

        self.user = user


class LoginEmailForm(forms.Form):
    username = forms.CharField(max_length=150, required=False)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput, required=False)
    email = forms.EmailField(widget=forms.EmailInput, required=False)

    def clean(self):
        cleaned_data = super(LoginEmailForm, self).clean()
        if not cleaned_data['email'] and not (cleaned_data['username'] and cleaned_data['password']):
            raise forms.ValidationError(
                'Please fill up one of forms.'
            )

        user_model = get_user_model()
        user = user_model._default_manager.filter(
            username=cleaned_data['username'],
            password=cleaned_data['password']
        ).first() or user_model._default_manager.filter(
            email=cleaned_data['email']
        ).first()

        if not user:
            raise forms.ValidationError(
                'Account with these credentials not exists.'
            )

        self.user = user
