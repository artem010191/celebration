from django.contrib import admin
from django.urls import path, include
from apps.main import urls as main_urls
from apps.login import urls as login_urls
from apps.registration import urls as reg_urls
from apps.rest import urls as rest_urls
from apps.account import urls as account_urls
from apps.activation import urls as act_urls


urlpatterns = [
    path('', include(main_urls)),
    path('activation/', include(act_urls)),
    path('account/', include(account_urls)),
    path('login/', include(login_urls)),
    path('registration/', include(reg_urls)),
    path('rest/', include(rest_urls)),
    path('admin/', admin.site.urls),
]
