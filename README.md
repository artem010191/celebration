# Celebration
##Предзапуск:
    1. Установить пакеты из requirements.txt.
    2. В apps/main/migrations/0001_initial.py задать domain для site framework.
        (По умолчанию localhost:8000)
    3. Выполнить миграции.
        (В случае ValueError: Related model 'main.User' cannot be resolved
        выполнить `python manage.py migrate main`, а после `python manage.py migrate`.) 
    4. Задать параметры для email рассылки в settings.py.
        (Для локали `EMAIL_HOST = 'localhost'`, `EMAIL_PORT = '1025'`.
         В командной строке прописать `python -m smtpd -n -c DebuggingServer localhost:1025`)
    5. Для Celery в settings.py указать CELERY_BROKER_URL.
        (Для локали: `CELERY_BROKER_URL = 'amqp://localhost'`.
         Для Ubuntu: Поставить rebbitmq сервер: `apt-get install -y erlang && apt-get install rabbitmq-server`.
         Запустить: `systemctl enable rabbitmq-server && systemctl start rabbitmq-server`.
         В консоли прописать: `celery -A celebration worker -l info`)
##API:
<a href='#GetTokens'>Обновить токены</a><br/>
<a href='#CelebrationsForMonth'>Получить праздники за месяц</a><br/>
<a href='#GetEventsByDate'>Получить праздники за месяц</a><br/>
<a href='#CreateEvent'>Создание события</a><br/>

Все запросы кроме тех в которых указан Authorization header трубуют `Authorization: Beaver <access_token>` header.<br/>
`access_token` можно получить в своем профайле `<domain>/account/`
    
###<label id='GetTokens'>Обновить токены</label>
Url: `<domain>/rest/token-pair/`<br/>
Метод: POST<br/>
HEADER: `Authorization: Beaver <refresh_token>`
Ответ:

    {'access_token': <access_token>, 'refresh_token': <refresh_token>}

###<label id='CelebrationsForMonth'>Получить праздники за месяц</label>
Url: `<domain>/rest/celebrations/`<br/>
Метод: Get<br/>
Параметры:<br/>

    month:
        тип: int(от 1 до 12)
        
Ответ:

    {<celebration_name>: {'begin': <begin_time>, 'end': <end_time>}}
    
###<label id='GetEventsByDate'>Фильтр событий</label>
Url: `<domain>/rest/events-by-date/`<br/>
Метод: Get<br/>
Параметры:<br/>
    
    Запрос принимает лишь один из параметров.
    
    month:
        тип: string
        формат: %Y-%m
    day:
        тип: string
        формат: %Y-%m-%d
Ответ:
    
    {event_name: {'event_start': event_start,'event_end': event_end}}
###<label id='CreateEvent'>Создание события</label>
Url: `<domain>/rest/events/`<br/>
Метод: POST<br/>
Параметры:<br/>
    
    Обязательные:
        event_name:
            тип: string
        event_start:
            тип: string
            формат: YYYY-MM-DDThh:mm[:ss[.uuuuuu]][+HH:MM|-HH:MM|Z]
    
    Не обязательные:
        event_end
            тип: string
            формат: YYYY-MM-DDThh:mm[:ss[.uuuuuu]][+HH:MM|-HH:MM|Z]
        
        reminders:
            тип: int
            choices: 0 = 'For hour',
                     1 = 'For 2 hours',
                     2 = 'For 4 hours',
                     3 = 'For day',
                     4 = 'For week'
Ответ:
    {
